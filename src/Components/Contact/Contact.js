import React, { Component } from "react";
import "./Contact.css";

class Contact extends Component {
  render() {
    return (
      <div className="flex-container">
        <div>{this.props.name}</div>
        <div>{this.props.phone}</div>
        <div>
          <div className="delete-button" onClick={this.props.delete}>
            DELETE
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
