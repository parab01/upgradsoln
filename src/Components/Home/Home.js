import React, { Component } from "react";
import "./Home.css";
import { Link } from "react-router-dom";
import Contact from "../Contact/Contact";

class Home extends Component {
  deleteContact = index => {
    console.log(index);
    console.log("works");
    //now I just need to add and delete the by the index
    this.props.deleteContact(index);
  };

  render() {
    return (
      <div>
        <Link to="/add">
          <div className="add-btn">ADD</div>
        </Link>
        <div className="flex-container">
          <div>NAME</div>
          <div>PHONE</div>
          <div />
        </div>
        {this.props.contacts.map((contact, index) => {
          return (
            <Contact
              key={index}
              name={contact.name}
              phone={contact.phone}
              delete={this.deleteContact.bind(this, index)}
            />
          );
        })}
      </div>
    );
  }
}

export default Home;
